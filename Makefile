NAME = source-date-epoch-spec
TARGETS = $(NAME).html

all: $(TARGETS)

%.html: %.xml
	xmlto -x $(NAME).xsl html-nochunks $<

clean:
	rm -f $(TARGETS)
